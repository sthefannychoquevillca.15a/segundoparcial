/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Segundoparcial;

/**
 *
 * @author Mix
 */
public class ExamenCola {

    private Nodo first;
    private Nodo last;
    private int size;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        if(this.contar()<=size)
            this.size = size;
        else
            System.out.println("la cola tiene mas elementos que el tamaño al que quiere cambiar");
    }
    public ExamenCola() {
        first = null;
        last = null;
        size = -1;
    }
    public ExamenCola(int size) {
        first = null;
        last = null;
        this.size = size;
    }

    public void insertar(Object dato) {
        Nodo i = new Nodo(dato);
        i.setNext(null);
        if (first == null & last == null) {
            first = i;
            last = i;
        }
        else{
            last.setNext(i);
            last = last.getNext();
        }
    }

    public Object extraer() {
        Object dato = first.getDato();
        first = first.getNext();
        if(first == null)
            last = null;
        return dato;
    }

    public boolean estaVacia() {
        boolean cola = false;
        if (first == null & last == null) {
            cola = true;
        } else {
            cola = false;
        }
        return cola;
    }
    
    public boolean estaLLena(){
        return (this.contar()==size);
    }

    public int contar() {
        int contador = 0;
        Nodo c = this.first;
        while (c != null) {
            contador++;
            c = c.getNext();
        }
        return contador;
    }
    
    public String toString() {
        Nodo c = this.first;
        String s = "";
        while (c != null) {
            s = s + c.toString();
            c = c.getNext();
        }
        return s;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Segundoparcial;

/**
 *
 * @author Erwin
 */
public class ExamenPila {
    private Nodo last;
    private int size;
    public ExamenPila() {
        last = null;
        size = -1;
    }
    public ExamenPila(int size) {
        last = null;
        this.size=size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void insertar(Object dato) {
        if(!this.estaLlena()){
            Nodo i = new Nodo(dato);
            i.setNext(last);
            last = i;
        } else {
            System.out.println("La pila esta llena");
        }
    }

    public Object extraer() {
        Object dato = last.getDato();
        last = last.getNext();
        return dato;
    }

    public boolean estaVacia() {
        
        return last==null;
    }
    public boolean estaLlena() {
        
        return (size == this.contar());
    }
    public int contar() {
        int contador = 0;
        Nodo c = this.last;
        while (c != null) {
            contador++;
            c = c.getNext();
        }
        return contador;
    }
    
    public String toString() {
        Nodo c = this.last;
        String s = "";
        while (c != null) {
            s = s +" ["+ c.toString()+c.getDato().getClass().getSimpleName()+" ] ";
            c = c.getNext();
        }
        return s;
    }
}

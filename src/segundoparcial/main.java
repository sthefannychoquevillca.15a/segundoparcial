package Segundoparcial;

import java.util.Scanner;

/**
 *
 * @author Mix
 */
public class main {
    static ExamenCola cola;
    public static void main(String[] args) {
//        Examencolas1 res = new Examencolas1();
//        res.insertar('z');
//        res.insertar("tarea");
//        res.insertar("hola");
//        res.insertar(15);
//        res.insertar(4);
//        res.extraer();
//        res.estaVacia();
//        res.contar();
//        System.out.println(res.toString());


        Scanner en = new Scanner(System.in);
        System.out.println("Tipo de Cola");
        System.out.println("1.- Dinamico");
        System.out.println("2.- Limite");
        int op = en.nextInt();
        if(op == 1){
            cola = new ExamenCola();
        } else {
            System.out.println("Tamaño de la cola?: ");
            cola = new ExamenCola(en.nextInt());
        }
        boolean exit = false;
        while(!exit){
            menu();
            op=en.nextInt();
            switch(op){
                case 1:
                    insertar(en);
                    break;
                case 2:
                    borrar(en);
                    break;
                case 3:
                    cambiarLimite(en);
                    break;
                case 4:
                    mostrarNumeroDeElementos();
                    break;
                case 5:
                    mostrarElementoMinMax();
                    break;
                case 6:
                    buscarUnElemento(en);
                    break;
                case 7:
                    mostrarElementos();
                    break;
                case 8:
                    ordenarElementos(en);
                    break;
                case 9:
                    mostrarElementosConSusTipos();
                    break;
                case 10:
                    SepararPorTipos(en);
                    break;
                case 11:
                    SacarTodosLosElementos();
                    break;
                case 12:
                    
                    exit = true;
                    break;
                default:
                    System.out.println("No es opcion!!!");
            }
        }
    }
    public static void menu(){
        System.out.println("[============================== - MENU - ==============================]");
        System.out.println("1.- Añadir un nuevo elemento"); 
        System.out.println("2.- Borrar un elemento de la estructura cola"); 
        System.out.println("3.- Cambiar el limite de la cola"); 
        System.out.println("4.- Mostrar el numero de elementos que existe en la estrucutra cola"); 
        System.out.println("5.- Mostrar el elemeto minimo y maximo"); 
        System.out.println("6.- Buscar un elemeto dentro de la estructura cola");
        System.out.println("7.- Mostrar todos los elementos"); 
        System.out.println("8.- Ordenar los elemetos de forma acendente o decendente");
        System.out.println("9.- Mostrar todos los elemetos con su tipo de dato"); 
        System.out.println("10.- Separar los tipos de datos en estructura de pilas que estos esten dentro de una estrucutra cola"); 
        System.out.println("11.- Sacar todos los elemtos");
        System.out.println("12.- Salir"); 
        System.out.println("[======================================================================]");
        System.out.print("Opcion: ");
    }

    private static void insertar(Scanner en) {
        if(!cola.estaLLena()){
            System.out.println("Introdusca un dato");
            cola.insertar(leerObjeto(en));
        } else {
            System.out.println("la cola esta llena!");
        }
    }
    public static Object leerObjeto(Scanner en){
        Object o = null;
            try{
                o = (en.nextInt());
            }
            catch(Exception eInt){
                try{
                    o = (en.nextFloat());
                }
                catch(Exception eFloat){
                    try{
                        o = (en.nextBoolean());
                    }
                    catch(Exception eBoolean){
                        o = (en.next());
                    }
                }
            }
        return o;
    }
    private static void borrar(Scanner en) {
        if(!cola.estaVacia()){
            System.out.println("Que dato decea eliminar?: ");
            Object o = leerObjeto(en);
            ExamenCola aux = new ExamenCola(cola.getSize());
            boolean eliminado = false;
            while(!cola.estaVacia()){
                Object dato = cola.extraer();
                if(!dato.equals(o)||eliminado){
                    aux.insertar(dato);
                } else {
                    eliminado = true;
                }
            }
            cola = aux;
        } else {
            System.out.println("la cola esta vacia!");
        }
    }

    private static void cambiarLimite(Scanner en) {
        System.out.print("Nuevo tamaño: ");
        cola.setSize(en.nextInt());
    }

    private static void mostrarElementoMinMax() {
        ExamenCola aux = new ExamenCola(cola.getSize());
        Object min=null,max=null;
        boolean primero = true;
        while(!cola.estaVacia()){
            Object dato = cola.extraer();
            if(primero){
                primero = false;
                min = dato;
                max = dato;
            } else {
                if(min.toString().compareTo(dato.toString())>0)
                    min = dato;
                if(max.toString().compareTo(dato.toString())<0)
                    max = dato;
            }
            aux.insertar(dato);
        }
        cola = aux;
        if(min == null & max == null){
            System.out.println("La cola esta vacia!");
        }
        else{
            System.out.println("Minimo: "+min.toString()+" Maximo: "+max.toString());
        }
    }

    private static void mostrarNumeroDeElementos() {
        System.out.println("Cantidad de elementos: "+cola.contar());
    }

    private static void buscarUnElemento(Scanner en) {
        ExamenCola aux = new ExamenCola(cola.getSize());
        System.out.print("introduzca el elemento a buscar: ");
        Object buscar=leerObjeto(en);
        boolean encontrado = false;
        int posicion = 0;
        while(!cola.estaVacia()){
            Object dato = cola.extraer();
            if(!encontrado) posicion++;
            if(dato.toString().equals(buscar.toString())){
                encontrado = true;
            }
            aux.insertar(dato);
        }
        cola = aux;
        if(encontrado){
            System.out.println("\nEl elemento "+buscar.toString()+" fe encontrado en la posicion "+posicion);
        }
        else{
            System.out.println("El elemento no existe en la cola");
        }
    }

    private static void mostrarElementos() {
        System.out.println("Datos de la cola: \n"+cola.toString());
    }

    private static void ordenarElementos(Scanner en) {
        System.out.println("Como desea ordenar?");
        System.out.println("1.- Acendente");
        System.out.println("2.- Decendente");
        int op = en.nextInt();
        ExamenCola colaOrdenada=new ExamenCola(cola.getSize());
        while(!cola.estaVacia()){
            Object d = cola.extraer();
            if(colaOrdenada.estaVacia()){
                colaOrdenada.insertar(d);
            } else {
                boolean insertado = false;
                boolean insertar = false;
                ExamenCola aux=new ExamenCola(colaOrdenada.getSize());
                while(!colaOrdenada.estaVacia()){
                    Object dco = colaOrdenada.extraer();
                    insertar = (op==1? (dco.toString().compareTo(d.toString())>0) : (dco.toString().compareTo(d.toString())<0) );
                    if(insertar){
                        if(!insertado){
                            insertado =true;
                            aux.insertar(d);
                        }
                    }
                    aux.insertar(dco);
                }
                if(!insertado)
                    aux.insertar(d);
                colaOrdenada = aux;
            }
        }
        cola = colaOrdenada;
    }

    private static void mostrarElementosConSusTipos() {
        System.out.println("los datos son: ");
        ExamenCola aux = new ExamenCola(cola.getSize());
        while(!cola.estaVacia()){
            Object d = cola.extraer();
            System.out.print(" [ "+d.toString()+" - "+d.getClass().getSimpleName()+" ] ");
            aux.insertar(d);
        }
        System.out.println();
        cola = aux;
    }

    private static void SepararPorTipos(Scanner en) {
        System.out.print("- cuantos datos desea separar: ");
        int tamPila = en.nextInt();
        ExamenCola aux = new ExamenCola(cola.getSize());
        ExamenCola colaConPilas = new ExamenCola();
        while(!cola.estaVacia()){
            Object data = cola.extraer();
            aux.insertar(data);
            boolean apilado = false;
            ExamenCola auxCP = new ExamenCola(cola.getSize());
            while(!colaConPilas.estaVacia()){
                ExamenPila p =(ExamenPila)colaConPilas.extraer();
                Object pd = p.extraer();
                p.insertar(pd);
                if(pd.getClass() == data.getClass()&&!p.estaLlena()&&!apilado){
                    p.insertar(data);
                    apilado = true;
                }
                auxCP.insertar(p);
            }
            if(!apilado){
                ExamenPila p=new ExamenPila(tamPila);
                p.insertar(data);
                auxCP.insertar(p);
            }
            colaConPilas = auxCP;
        }
        cola = aux;
        // extraer todos lo selementos de las pilas que estan en la cola
        int cont = 0;
        while(!colaConPilas.estaVacia()){
            ExamenPila p =(ExamenPila) colaConPilas.extraer();
            Object d = p.extraer();
            System.out.print(++cont+" - "+d.getClass().getSimpleName()+" : ");
            System.out.print(d.toString()+"  ");
            while(!p.estaVacia()){
                d = p.extraer();
                System.out.print(d.toString()+"  ");
            }
            System.out.println();
        }
    }

    private static void SacarTodosLosElementos() {
        while(!cola.estaVacia()){
            Object d = cola.extraer();
            System.out.println(" "+d.toString()+" ");
        }
        System.out.println();
    }

}
